#ifndef COLOR
#define COLOR

void RGB2YCC(int R,int G,int B, int &Y, int &Cb, int &Cr);

void YCC2RGB(int Y,int Cb,int Cr, int &R, int &G, int &B);

void RGB2YCCjpeg(int R,int G,int B, int &Y, int &Cb, int &Cr);

void YCC2RGBjpeg(int Y,int Cb,int Cr, int &R, int &G, int &B);

#endif // COLOR

