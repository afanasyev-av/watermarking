#include<math.h>

void DCT(double **source, double **result, int m,int n)
{
    int p,q;
    double alphaP,alphaQ;
    int i,j;

    for(p=0;p<m;p++){
        for(q=0;q<n;q++){
            if(p==0){
                alphaP = sqrt(1.0/m);
            }
            else{
                alphaP = sqrt(2.0/m);
            }
            if(q==0){
                alphaQ = sqrt(1.0/n);
            }
            else{
                alphaQ = sqrt(2.0/n);
            }
            result[p][q] = 0.0;
            for(i=0;i<m;i++){
                for(j=0;j<n;j++){
                    result[p][q] = result[p][q] +
                            alphaP*alphaQ*source[i][j]*
                                cos(M_PI*(2.0*i+1.0)*(double)p/(2.0*m))*cos(M_PI*(2.0*j+1.0)*(double)q/(2.0*n));
                }
            }
        }
    }
}

void IDCT(double **result, double **source, int m,int n)
{
    int p,q;
    double alphaP,alphaQ;
    int i,j;

    for(i=0;i<m;i++){
        for(j=0;j<n;j++){
            source[i][j] = 0;
            for(p=0;p<m;p++){
                for(q=0;q<n;q++){
                    if(p==0){
                        alphaP = sqrt(1.0/m);
                    }
                    else{
                        alphaP = sqrt(2.0/m);
                    }
                    if(q==0){
                        alphaQ = sqrt(1.0/n);
                    }
                    else{
                        alphaQ = sqrt(2.0/n);
                    }
                    source[i][j] = source[i][j] +
                            alphaP*alphaQ*result[p][q]*
                                cos(M_PI*(2.0*i+1.0)*(double)p/(2.0*m))*cos(M_PI*(2.0*j+1.0)*(double)q/(2.0*n));
                }
            }
        }
    }
}

void FFT(double **source, double **Mres, double **Pres, int n, int m)
{
    double **Re,**Im;
    int i,j;
    int p,q;
    Re = new double*[n];
    Im = new double*[n];
    for(i=0;i<n;i++){
        Re[i]=new double[m];
        Im[i]=new double[m];
    }

    for(p=0;p<n;p++){
        for(q=0;q<m;q++){
            Re[p][q]=0;
            Im[p][q]=0;

            for(i=0;i<n;i++){
                for(j=0;j<m;j++){
                    Re[p][q] = Re[p][q]+source[i][j]*cos(2.0*M_PI*((double)p*i/n+(double)q*j/m));
                    Im[p][q] = Im[p][q]+source[i][j]*sin(2.0*M_PI*((double)p*i/n+(double)q*j/m));
                }
            }

            Mres[p][q] = Re[p][q]*Re[p][q]+Im[p][q]*Im[p][q];
            Pres[p][q] = atan2(Im[p][q],Re[p][q]);
        }
    }

    for(i=0;i<n;i++){
        delete[] Re[i];
        delete[] Im[i];
    }
    delete Re;
    delete Im;
}

void IFFT(double **Mres, double **Pres, double **source, int n, int m)
{
    int i,j;
    int p,q;

    for(i=0;i<n;i++){
        for(j=0;j<m;j++){
            source[i][j]=0;
            for(p=0;p<n;p++){
                for(q=0;q<m;q++){
                    source[i][j] = source[i][j]+1.0/n/m*Mres[p][q]*cos(Pres[p][q]);
                }
            }
        }
    }
}
