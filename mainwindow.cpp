#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
//открываем оригинальное изображение
void MainWindow::on_actionOpen_File_triggered()
{
    //QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("Jpeg file (*.jpg)"));
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("PNG file (*.png)"));
    QPixmap pix;
    Originimage = new QImage(FileName);
    int N,M;
    N = Originimage->height();
    M = Originimage->width();

    pix.load(FileName);
    ui->OriginalImage->setPixmap(pix);
}
//сохраняем изображение с водяным знаком
void MainWindow::on_actionSave_File_triggered()
{
    //QString FileName = QFileDialog::getSaveFileName(this, tr("Save File"),"",tr("Jpeg file (*.jpg)"));
    QString FileName = QFileDialog::getSaveFileName(this, tr("Save File"),"",tr("PNG file (*.png)"));
    ui->ResultImage->pixmap()->save(FileName);
}
//добавление водяного знака 2 алгоритм
void MainWindow::on_actionLuminance_Component_Embedding_triggered()
{
    QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("PNG file (*.png)"));
    QImage watermark = QImage(FileName);
    int C;
    C = 1;

    //размеры исходного изображения
    int N,M;
    N = Originimage->height();
    M = Originimage->width();

    //размеры водяного знака
    int h,w;
    h = watermark.height();
    w = watermark.width();

    QImage Resultimage(M,N,QImage::Format_RGB32);

    int Cb[8][8],Cr[8][8];
    double **dct;
    double **Y;
    double temp;
    bool water;
    int y;
    Y = new double*[8];
    dct = new double*[8];
    for(int i=0;i<8;i++){
        Y[i] = new double[8];
        dct[i] = new double[8];
    }
    int R,G,B;
    int i,j;
    int k,l;
    //строим сетку и для каждого элемента сетки (матрица 8*8) применяем преобразование
    for(i=0;i<N/8;i++){
        for(j=0;j<M/8;j++){
            //переводим из RGB в YCC
            for(k=0;k<8;k++){
                for(l=0;l<8;l++){
                    R = qRed(Originimage->pixel(i*8+k,j*8+l));
                    G = qGreen(Originimage->pixel(i*8+k,j*8+l));
                    B = qBlue(Originimage->pixel(i*8+k,j*8+l));
                    RGB2YCCjpeg(R,G,B,y,Cb[k][l],Cr[k][l]);
                    Y[k][l] = y;
                    qDebug() << Y[k][l] << " ";
                }
                qDebug() << "\n";
            }
            //дискретное преобразование Y
            DCT(Y,dct,8,8);

            //добавляем водяной знак
            water = qRed(watermark.pixel(i,j))!=255;

            if(water && (dct[5][2] >= dct[4][3])){
                dct[5][2]= dct[5][2]+C;
            }
            if(water && (dct[5][2] < dct[4][3])){
                temp = dct[5][2];
                dct[5][2] = dct[4][3];
                dct[4][3] = temp;
                dct[5][2] = dct[5][2]+C;
            }
            if(!water && (dct[4][3] >= dct[5][2])){
                dct[4][3] = dct[4][3]+C;
            }
            if(!water && (dct[4][3] < dct[5][2])){
                temp = dct[4][3];
                dct[4][3] = dct[5][2];
                dct[5][2] = temp;
                dct[4][3]= dct[4][3]+C;
            }

            //обратное преобразование Y
            IDCT(dct,Y,8,8);
            //переводим из YCC в RGB
            for(k=0;k<8;k++){
                for(l=0;l<8;l++){
                    qDebug() << Y[k][l] << " ";
                    y = Y[k][l];
                    YCC2RGBjpeg(y,Cb[k][l],Cr[k][l],R,G,B);
                    Resultimage.setPixel(i*8+k,j*8+l,qRgb(R,G,B));
                }
                qDebug() << "\n";
            }
        }
    }

    QPixmap pix(M,N);
    pix = QPixmap::fromImage(Resultimage);
    ui->ResultImage->setPixmap(pix);

    for(int i=0;i<8;i++){
        delete[] Y[i];
        delete[] dct[i];
    }
    delete[] Y;
    delete[] dct;
}
//выделение водяного знака алгоритм 2
void MainWindow::on_actionLuminance_Component_Extraction_triggered()
{
    //размеры изображения
    int N,M;
    N = Originimage->height();
    M = Originimage->width();

    //образ водяного знака
    QImage watermark(M/8,N/8,QImage::Format_RGB32);

    int Cb[8][8],Cr[8][8];
    double **dct;
    double **Y;
    int y;
    Y = new double*[8];
    dct = new double*[8];
    for(int i=0;i<8;i++){
        Y[i] = new double[8];
        dct[i] = new double[8];
    }
    int R,G,B;
    int i,j;
    int k,l;
    //строим сетку и для каждого элемента сетки (матрица 8*8) применяем преобразование
    for(i=0;i<N/8;i++){
        for(j=0;j<M/8;j++){
            //переводим из RGB в YCC
            for(k=0;k<8;k++){
                for(l=0;l<8;l++){
                    R = qRed(Originimage->pixel(i*8+k,j*8+l));
                    G = qGreen(Originimage->pixel(i*8+k,j*8+l));
                    B = qBlue(Originimage->pixel(i*8+k,j*8+l));
                    RGB2YCCjpeg(R,G,B,y,Cb[k][l],Cr[k][l]);
                    Y[k][l] = y;
                    qDebug() << Y[k][l] << " ";
                }
                qDebug() << "\n";
            }
            //косинусоидальное преобразование Y
            DCT(Y,dct,8,8);
            //выделяем водяной знак
            if(dct[5][2] > dct[4][3] ){watermark.setPixel(i,j,qRgb(0,0,0));}
            if(dct[5][2] < dct[4][3] ){watermark.setPixel(i,j,qRgb(255,255,255));}
        }
    }

    QPixmap pix(M,N);
    pix = QPixmap::fromImage(watermark);
    ui->ResultImage->setPixmap(pix);

    for(int i=0;i<8;i++){
        delete[] Y[i];
        delete[] dct[i];
    }
    delete[] Y;
    delete[] dct;
}
//алгоритм 1 не доделан
void MainWindow::on_actionSelf_Inverting_Embedding_triggered()
{
    int N,M;
    N = Originimage->height();
    M = Originimage->width();
    vector<int> watermark;
    watermark.push_back(6);
    watermark.push_back(3);
    watermark.push_back(2);
    watermark.push_back(4);
    watermark.push_back(5);
    watermark.push_back(1);
    int n;
    n= watermark.size();

    double **R,**G,**B;
    double **Mcur,**Pcur;
    double **Mr,**Mg,**Mb;
    double **Pr,**Pg,**Pb;
    int i,j,p,q;

    R = new double*[N/n];
    G = new double*[N/n];
    B = new double*[N/n];
    Mcur = new double*[N/n];
    Pcur = new double*[N/n];
    Mr = new double*[N];
    Mg = new double*[N];
    Mb = new double*[N];

    Pr = new double*[N];
    Pg = new double*[N];
    Pb = new double*[N];

    for(int i=0;i<N/n;i++){
        R[i] = new double[M/n];
        G[i] = new double[M/n];
        B[i] = new double[M/n];
        Mcur[i] = new double[M/n];
        Pcur[i] = new double[M/n];
    }

    for(int i=0;i<N;i++){
        Mr[i] = new double[M];
        Mg[i] = new double[M];
        Mb[i] = new double[M];

        Pr[i] = new double[M];
        Pg[i] = new double[M];
        Pb[i] = new double[M];
    }


    //for(i=0;i<n;i++){
        //for(j=0;j<n;j++){
            //for()
            //R[][] = qRed(Originimage->pixel(i*8+k,j*8+l));
            //G = qGreen(Originimage->pixel(i*8+k,j*8+l));
            //B = qBlue(Originimage->pixel(i*8+k,j*8+l));


    for(int i=0;i<N;i++){
        delete [] R[i];
        delete [] G[i];
        delete [] B[i];
        delete [] Mr[i];
        delete [] Mg[i];
        delete [] Mb[i];

        delete [] Pr[i];
        delete [] Pg[i];
        delete [] Pb[i];
    }
    delete [] R;
    delete [] G;
    delete [] B;
    delete [] Mr;
    delete [] Mg;
    delete [] Mb;

    delete [] Pr;
    delete [] Pg;
    delete [] Pb;
}
