#-------------------------------------------------
#
# Project created by QtCreator 2016-05-08T11:47:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = watermarking
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    color.cpp \
    transform.cpp

HEADERS  += mainwindow.h \
    color.h \
    transform.h

FORMS    += mainwindow.ui

#LIBS += \
#       -lboost_system\
