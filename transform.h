#ifndef TRANSFORM
#define TRANSFORM

void DCT(double **source, double **result, int n,int m);
void IDCT(double **result, double **source, int n,int m);

void FFT(double **source, double **Mres, double **Pres, int n, int m);
void IFFT(double **Mres, double **Pres, double **source, int n, int m);

#endif // TRANSFORM

