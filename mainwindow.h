#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include "color.h"
#include "transform.h"
#include <QDebug>
#include <vector>
using namespace std;
//#include "boost/multi_array.hpp"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_File_triggered();
    
    void on_actionSave_File_triggered();

    void on_actionLuminance_Component_Embedding_triggered();

    void on_actionLuminance_Component_Extraction_triggered();

    void on_actionSelf_Inverting_Embedding_triggered();

private:
    Ui::MainWindow *ui;
    QImage *Originimage;
    //QImage *Resultimage;
};

#endif // MAINWINDOW_H
